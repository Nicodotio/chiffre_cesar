import string


def chiffrement_phrase(phrase_saisie, decalage):
    phrase_chiffree = ""
    decalage = int(decalage)
    for caractere in phrase_saisie:
        valeur_ascii_caractere = ord(caractere)
        valeur_ascii_caractere_chiffree = valeur_ascii_caractere+decalage
        caractere_chiffree = chr(valeur_ascii_caractere_chiffree)
        phrase_chiffree = "".join([phrase_chiffree, caractere_chiffree])
    return phrase_chiffree


def dechiffrement_phrase(phrase_saisie, decalage):
    phrase_dechiffree = ""
    decalage = int(decalage)
    for caractere in phrase_saisie:
        valeur_ascii_caractere = ord(caractere)
        valeur_ascii_caractere_dechiffree = valeur_ascii_caractere-decalage
        caractere_dechiffree = chr(valeur_ascii_caractere_dechiffree)
        phrase_dechiffree = "".join([phrase_dechiffree, caractere_dechiffree])
    return phrase_dechiffree


def chiffrement_phrase_circulaire(phrase_saisie, decalage):
    phrase_chiffree = ""
    decalage = int(decalage)
    lettres_minuscules = string.ascii_lowercase
    lettres_majuscules = string.ascii_uppercase
    lettres_minuscules_decalage = lettres_minuscules[decalage:] + \
        lettres_minuscules[:decalage]
    lettres_majuscules_decalage = lettres_majuscules[decalage:] + \
        lettres_majuscules[:decalage]
    for caractere in phrase_saisie:
        valeur_ascii_caractere = ord(caractere)
        if((valeur_ascii_caractere > 96) & (valeur_ascii_caractere < 123)):
            caractere_chiffree = lettres_minuscules_decalage[valeur_ascii_caractere-97]
        elif((valeur_ascii_caractere > 64) & (valeur_ascii_caractere < 91)):
            caractere_chiffree = lettres_majuscules_decalage[valeur_ascii_caractere-65]
        else:
            caractere_chiffree = caractere
        phrase_chiffree = "".join([phrase_chiffree, caractere_chiffree])
    return phrase_chiffree


def dechiffrement_phrase_circulaire(phrase_saisie, decalage):
    raise NotImplementedError
