from tkinter import *
from tkinter.ttk import *
from chiffre_cesar import chiffrement_phrase, dechiffrement_phrase
from chiffre_cesar import chiffrement_phrase_circulaire


def clic_btn_chiffrer():
    saisie_utilisateur = entry_saisie.get()
    retour_saisie_utilisateur = chiffrement_phrase(
        saisie_utilisateur, entry_decalage.get())
    resultat.config(text=retour_saisie_utilisateur)


def clic_btn_dechiffrer():
    saisie_utilisateur = entry_saisie.get()
    retour_saisie_utilisateur = dechiffrement_phrase(
        saisie_utilisateur, entry_decalage.get())
    resultat.config(text=retour_saisie_utilisateur)


def clic_btn_chiffrer_circulairement():
    saisie_utilisateur = entry_saisie.get()
    retour_saisie_utilisateur = chiffrement_phrase_circulaire(
        saisie_utilisateur, entry_decalage.get())
    resultat.config(text=retour_saisie_utilisateur)


frame = Tk()
frame.title("Chiffre César")
frame.rowconfigure(0, weight=1)
frame.rowconfigure(1, weight=1)
frame.rowconfigure(2, weight=1)
frame.rowconfigure(3, weight=1)
frame.rowconfigure(4, weight=1)
frame.rowconfigure(5, weight=1)
frame.rowconfigure(6, weight=1)
frame.rowconfigure(7, weight=1)
frame.columnconfigure(0, weight=1)
frame.columnconfigure(1, weight=1)

value = StringVar()

label_saisie = Label(frame, text="Saisir le texte :")
label_saisie.grid(row=0, column=0, columnspan=2)

entry_saisie = Entry(frame)
entry_saisie.grid(row=1, column=0, columnspan=2)

label_decalage = Label(frame, text="Saisir le décalage :")
label_decalage.grid(row=2, column=0, columnspan=2)

entry_decalage = Spinbox(frame, textvariable=IntVar(), from_=1, to=25)
entry_decalage.grid(row=3, column=0, columnspan=2)

button_chiffrer = Button(frame, text="Chiffrer", command=clic_btn_chiffrer)
button_chiffrer.grid(row=4, column=0)

button_dechiffrer = Button(frame, text="Déchiffrer",
                           command=clic_btn_dechiffrer)
button_dechiffrer.grid(row=4, column=1)

button_chiffrer_circulaire = Button(
    frame, text="Chiffrer circulairement", command=clic_btn_chiffrer_circulairement)
button_chiffrer_circulaire.grid(row=5, column=0)

button_dechiffrer_circulaire = Button(
    frame, text="Déchiffrer circulairement", command=clic_btn_dechiffrer)
button_dechiffrer_circulaire.grid(row=5, column=1)

label = Label(frame, text="Voici le résultat :")
label.grid(row=6, column=0, columnspan=2)

resultat = Label(frame)
resultat.grid(row=7, column=0, columnspan=2)

frame.mainloop()
