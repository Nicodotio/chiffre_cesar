import unittest

from chiffre_cesar import chiffrement_phrase, dechiffrement_phrase
from chiffre_cesar import chiffrement_phrase_circulaire, dechiffrement_phrase_circulaire


class TestChiffreCesarFunctions(unittest.TestCase):
    def test_chiffrement_phrase(self):
        self.assertEqual(chiffrement_phrase("abc", 8), "ijk")
        self.assertEqual(chiffrement_phrase("dli", 8), "ltq")
        self.assertEqual(chiffrement_phrase("AMO", 8), "IUW")
        self.assertEqual(chiffrement_phrase("abc", 2), "cde")

    def test_dechiffrement_phrase(self):
        self.assertEqual(dechiffrement_phrase("owt", 8), "gol")
        self.assertEqual(dechiffrement_phrase("ijl", 8), "abd")
        self.assertEqual(dechiffrement_phrase("MNV", 8), "EFN")
        self.assertEqual(dechiffrement_phrase("elj", 1), "dki")

    def test_chiffrement_phrase_circulare(self):
        self.assertEqual(chiffrement_phrase_circulaire("xyz", 1), "yza")
        self.assertEqual(chiffrement_phrase_circulaire("abc", 25), "zab")
        self.assertEqual(chiffrement_phrase_circulaire("XYZ", 1), "YZA")
        self.assertEqual(chiffrement_phrase_circulaire("ABC", 25), "ZAB")
        self.assertEqual(chiffrement_phrase_circulaire("Z A", 1), "A B")

    def test_dechiffrement_phrase_circulare(self):
        self.assertEqual(dechiffrement_phrase_circulaire("abc", 1), "yza")
        self.assertEqual(dechiffrement_phrase_circulaire("zab", 25), "abc")
        self.assertEqual(dechiffrement_phrase_circulaire("ABC", 1), "YZA")
        self.assertEqual(dechiffrement_phrase_circulaire("ZAB", 25), "ABC")
